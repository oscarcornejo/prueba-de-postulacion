import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TablaNumerosComponent } from './pages/tabla-numeros/tabla-numeros.component';
import { TablaLetrasComponent } from './pages/tabla-letras/tabla-letras.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';



const routes: Routes = [
    {path: '', redirectTo: '/numeros', pathMatch: 'full' },
    {path: 'numeros', component: TablaNumerosComponent },
    {path: 'letras', component: TablaLetrasComponent },
    {path: '404', component: PageNotFoundComponent},
    {path: '**', redirectTo: '404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true,})],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
