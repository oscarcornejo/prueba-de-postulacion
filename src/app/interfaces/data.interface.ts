export interface IRemoteResponse {
    data?: any;
    error?: any;
    success: boolean;
}
