import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { IRemoteResponse } from 'src/app/interfaces/data.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-tabla-numeros',
  templateUrl: './tabla-numeros.component.html',
  styleUrls: ['./tabla-numeros.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TablaNumerosComponent implements OnInit, OnDestroy {

  numeros: any[] = [];
  arr1: any[] = [];
  arr2: any[] = [];
  arr3: any[];
  arr4: any[];
  mostrarMensaje: boolean = true;
  ordenarNumeros: any[] = [];
  loading: boolean;


  subscription = new Subscription();

  constructor(private dataApi: DataApiService, public cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    console.log('DATA: ', this.numeros);
  }

  obtenerNumeros() {
    this.loading = true;

    const subs = this.dataApi.getNumeros().subscribe( (resp: any) => {

      if (resp.data === null || resp.data === undefined) {
        this.mostrarMensaje = true;
        this.numeros = [];
        this.ordenarNumeros = [''];
        this.loading = false;
        this.cdRef.detectChanges();
        return;
      }

      if (resp.data !== null || resp.data !== undefined) {
        this.mostrarMensaje = false;
        this.numeros.push(resp.data);

        const dataLength = this.ordenarNumeros.length;
        for (let i = 0; i < dataLength; i++ ) {
          for (let j = 0; j < dataLength - 1 - i; j++ ) {
            if ( resp.data[ j ] > resp.data[ j + 1 ] ) {
              [ resp.data[ j ], resp.data[ j + 1 ] ] = [ resp.data[ j + 1 ], resp.data[ j ] ];
            }
          }
        }

        this.ordenarNumeros = resp.data;

        const arr = resp.data;
        const arr1 = [];
        const arr2 = [];
        const arr3 = [];
        const arr4 = [];
        let prev: any[] = [];

        arr.sort();

        for ( let i = 0; i < arr.length; i++ ) {
            if ( arr[i] !== prev ) {
              arr1.push(arr[i]);
              arr2.push(1);
              const position1 = arr.findIndex(x => x === arr[i]);
              arr3.push(position1);

              const element = arr[i];
              const idx = arr.lastIndexOf(element);
              arr4.push(idx);
            } else {
              arr2[arr2.length - 1]++;
            }
            prev = arr[i];
        }

        this.arr1 = arr1;
        this.arr2 = arr2;
        this.arr3 = arr3;
        this.arr4 = arr4;

        this.numeros = [this.arr1, this.arr2, this.arr3, this.arr4 ];

        this.mostrarMensaje = false;
        this.loading = false;
        this.cdRef.detectChanges();

      }
    });

    this.subscription.add(subs);

  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
