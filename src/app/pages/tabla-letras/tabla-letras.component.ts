import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tabla-letras',
  templateUrl: './tabla-letras.component.html',
  styleUrls: ['./tabla-letras.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TablaLetrasComponent implements OnInit, OnDestroy {

  letras: any[] = [];
  abcedario: any;

  filas: any[] = [];
  mostrarMensaje: boolean = true;
  cabeceras: any[];
  bodyTabla: any[] = [];

  loading: boolean;

  subscription = new Subscription();

  constructor(private dataApi: DataApiService, public cdRef: ChangeDetectorRef) { }

  ngOnInit() {}

  obtenerLetras() {
    this.loading = true;
    const subs = this.dataApi.getLetras().subscribe( (resp: any) => {

      if (resp.data === null || resp.data === undefined) {
        this.mostrarMensaje = true;
        this.loading = false;
        this.cdRef.detectChanges();
        return;
      }

      if (resp.data !== null || resp.data !== undefined) {
        this.letras = resp.data;

        const abc = 'abcdefghijklmnopqrstuvwxyz';
        for (let i = 0; i < resp.data.length; i++) {
            resp.data[i].paragraph = resp.data[i].paragraph.replace(/[^a-zA-Z]/g, '');
            resp.data[i].paragraph = resp.data[i].paragraph.toLowerCase();
        }

        this.letras = resp.data;

        this.filas = [];
        for (let i = 0; i < resp.data.length; i++) {
          this.abcedario = resp.data[i].paragraph.split('');
          this.cabeceras = abc.split('');

          if (this.abcedario.indexOf( abc[i] )) {
            this.filas = [];
            let count = 0;
            for (let x = 0; x < this.abcedario.length; x++) {
              const letra = this.abcedario[x];
              const idx = this.filas.findIndex( n => n.key === letra);

              if (idx !== null && idx !== -1 ) {
                const columna = this.filas[idx];
                columna.value++;
                this.filas[idx] = columna;
                count++;
              } else {
                const tabla = {
                  key: letra,
                  value:  1
                };
                this.filas.push(tabla);
                count++;
              }
              if (x === (this.abcedario.length - 1)) {
                const tabla = {
                  key: 'ñ',
                  value:  count
                };
                this.filas.push(tabla);
              }
            }
            this.filas = this.filas.filter(Boolean);
          }

          for (let y = 0; y < this.cabeceras.length; y++) {

            const letra = this.cabeceras[y];
            const idx = this.filas.findIndex( n => n.key === letra);

              if (idx === null || idx === -1 ) {
                const tabla = {
                  key: letra,
                  value:  0
                };
                this.filas.push(tabla);
              }

          }

          this.filas.sort(function(a, b) {
              if (a.key < b.key) { return -1; }

              if (a.key > b.key) { return 1; }
              return 0;
          });
          this.bodyTabla.push(this.filas);
        }

        this.mostrarMensaje = false;
        this.loading = false;
        this.cdRef.detectChanges();
      }
    });
    this.subscription.add(subs);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
