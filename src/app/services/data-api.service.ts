import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { IRemoteResponse } from '../interfaces/data.interface';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor(private http: HttpClient) { }

  getNumeros() {
    const url = 'http://168.232.165.184/prueba/array';
    return this.http.get<Array<IRemoteResponse>>(url);
  }

  getLetras() {
    const url = 'http://168.232.165.184/prueba/dict';
    return this.http.get<Array<IRemoteResponse>>(url);
    // .pipe(
    //   map( resp => {
    //     console.log('Resp: ', resp);
    //   })
    // );
  }

}
